from __future__ import division
from __future__ import print_function

import numpy as np

"""
Creates dice pixels
"""

def svg_dimensions(svg_file):
  # Get the width / height inside of the SVG
  from xml.dom import minidom
  doc = minidom.parse(svg_file)
  width = int([path.getAttribute('width') for path
               in doc.getElementsByTagName('svg')][0])
  height = int([path.getAttribute('height') for path
                in doc.getElementsByTagName('svg')][0])
  return width, height

def load_svg(filename, N):
  import array, cairo
  try:
    import rsvg   # requires python-rsvg
  except:
    print("Trying to get rsvg for Masha's computer")
    try:
      import gi
      gi.require_version('Rsvg', '2.0')
      from gi.repository import Rsvg as rsvg 
    except:
      print("Everything is terrible")
      raise
  width, height = svg_dimensions(filename)
  assert width==height, "Can't use {} as art. It is not square."
  img = cairo.ImageSurface(cairo.FORMAT_A8, N, N)
  try:
    svg = rsvg.Handle(filename)
  except:
    handle = rsvg.Handle()
    svg = handle.new_from_file(filename)
  ctx = cairo.Context(img)
  ctx.scale(N/width, N/height)
  svg.render_cairo(ctx)
  # The buffer seems insistent on having more than N^2 elements,
  # so truncate them in the most sensible way possible for now.
  # We *could* use the write_to_png method of ImageSurface, which
  # gives the expected size, but that might not be any better...
  data = np.frombuffer(img.get_data(), dtype=np.uint8)
  data = data.reshape((N, len(data)//N))
  return data[0:N,0:N]

#------------------------------------------------------------------------------

def drawDice(dice, N, thresh=255):
  """Draw the die in the pixelated region"""
  pixels = np.zeros((N,N), dtype=np.uint8)
  if (dice == 1):
    pixels[N//2, N//2]     = thresh
  elif (dice == 2):
    pixels[N//4, N//4]     = thresh
    pixels[3*N//4, 3*N//4] = thresh
  elif (dice == 3):
    pixels[N//4, N//4]     = thresh
    pixels[N//2, N//2]     = thresh
    pixels[3*N//4, 3*N//4] = thresh
  elif (dice == 4):
    pixels[N//4, N//4]     = thresh
    pixels[3*N//4, N//4]   = thresh
    pixels[N//4, 3*N//4]   = thresh
    pixels[3*N//4, 3*N//4] = thresh
  elif (dice == 5):
    pixels[N//4, N//4]     = thresh
    pixels[3*N//4, N//4]   = thresh
    pixels[N//2, N//2]     = thresh
    pixels[N//4, 3*N//4]   = thresh
    pixels[3*N//4, 3*N//4] = thresh
  elif (dice == 6):
    pixels[N//4, N//4]     = thresh
    pixels[N//4, N//2]     = thresh
    pixels[3*N//4, N//4]   = thresh
    pixels[N//4, 3*N//4]   = thresh
    pixels[3*N//4, N//2]   = thresh
    pixels[3*N//4, 3*N//4] = thresh
  return pixels

class DiceGraphics:
  def __init__(self, N, fromFile):
    """Class to efficiently draw graphical dice. Each die image is
    computed (or read from file and then rescaled to NxN pixels)."""
    if fromFile:
      self.dice = {
        1 : load_svg('Dice/Dice-1-b.svg', N),
        2 : load_svg('Dice/Dice-2.svg', N),
        3 : load_svg('Dice/Dice-3.svg', N),
        4 : load_svg('Dice/Dice-4-b.svg', N),
        5 : load_svg('Dice/Dice-5-b.svg', N),
        6 : load_svg('Dice/Dice-6a-b.svg', N),
      }
    else:
      self.dice = {
        1 : drawDice(1, N),
        2 : drawDice(2, N),
        3 : drawDice(3, N),
        4 : drawDice(4, N),
        5 : drawDice(5, N),
        6 : drawDice(6, N),
      }
  def Draw(self, i):
    """Return an NxNx3 array with the i-th die. The third dimension is
    needed since the rest of the interface assumes 3 color channels exist."""
    result = self.dice[i]
    return np.array([result, result, result]).transpose()

#------------------------------------------------------------------------------

class DiceLibrary:
  def __init__(self, N, fromFile=True):
    """Construct a library of dice, converting each SVG to a N x N x 3 array."""
    self.black = 255
    self.PicDic = {}
    dice = DiceGraphics(N, fromFile)
    for i in range(1,7):
      self.PicDic[i] = dice.Draw(i)

  def FindNearest(self, data):
    """Given a cell data array, find which die [1-6] the color value
    corresponds to, and return the die image as an NxN (greyscale)
    numpy array."""
    value = np.mean(data)
    binsize = self.black // 6
    die = (value // binsize) + 1
    return self.PicDic[die]
