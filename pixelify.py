#!/usr/bin/env python

from __future__ import division
from PIL import Image as Im
import sys, os
import numpy as np
from ImageLibrary import ImageLibrary
from DiceLibrary import DiceLibrary

#------------------------------------------------------------------------------

class PixelatedImage:
  def __init__(self, img, N):
    """Allow access to a PIL image in square cells of N^2 elements from left
    to right, then top to bottom. Use as an iterator to access each cell.
    Any extra pixels on the right and bottom that don't fit into an NxN cell
    will be lost. Third dimension is RGB values."""
    self.data = np.array(img)
    self.N = N
    self.new_cols = self.data.shape[0]//N
    self.new_rows = self.data.shape[1]//N
    self.result = np.zeros((self.new_cols*N, self.new_rows*N, 3),
                           dtype=np.uint8)
    self.col = -1 # current pixel column
    self.row = 0  # current pixel row

  def set_result_cell(self, new_data):
    """Sets the data for the current cell in the resulting image."""
    i = self.col
    j = self.row
    N = self.N
    self.result[i*N:(i+1)*N, j*N:(j+1)*N, :] = new_data

  def cell_data(self):
    """Returns the current cell of the image as an NxNx3 array."""
    i = self.col
    j = self.row
    N = self.N
    return self.data[i*N:(i+1)*N, j*N:(j+1)*N, :]

  def __iter__(self):
    """Returns the object as an iterator."""
    return self

  def next(self):
    """Increments the cell: by column first, then by row."""
    self.col = self.col + 1
    if self.col == self.new_cols:
      self.col = 0
      self.row = self.row + 1
    if self.row == self.new_rows:
      raise StopIteration
    else:
      return self

#------------------------------------------------------------------------------

def main():
  import argparse
  p = argparse.ArgumentParser()
  p.add_argument('master', nargs=1,
    help = "Master image to pixelize")
  p.add_argument('-N', default=15, type=int,
    help = "Size of each pixelated cell in the output image "
           "(default: %(default)s)")
  p.add_argument('-d', type=str,
    help = "Directory containing images to use as pixels. "
           "If omitted, will use dice as pixels.")
  p.add_argument('-o', type=str,
    help = "Output filename. Default is to append 'pixelified' to the input.")
  args = p.parse_args()

  ## Load in the image!
  infile = args.master[0]
  img = Im.open(infile).convert('RGB')

  ## Set up dice graphics pack
  N = args.N  # Length of cells in pixels
  if args.d:
    pixels = ImageLibrary(args.d, N, color=True)
  else:
    pixels = DiceLibrary(N, fromFile=True)

  ## Now pixelate and map values
  cells = PixelatedImage(img, N)
  for cell in cells:
    pixel = pixels.FindNearest(cell.cell_data())
    cell.set_result_cell(pixel)

  ## Convert the numpy array to a PIL image
  result = Im.fromarray(cells.result)

  ## Save the pixelified image
  if args.o:
    outfile = args.o
  else:
    basename, ext = os.path.splitext(infile)
    outfile = basename + '_pixelified' + ext
  result.save(outfile)


if __name__ == "__main__":
  main()
