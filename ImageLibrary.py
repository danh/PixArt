from __future__ import division
from __future__ import print_function

from PIL import Image as Im
from PIL import ImageEnhance as ImE
import sys
import numpy as np
import os 

#------------------------------------------------------------------------------

def RGBDistance(color1, color2):
  """ Computes a Euclidean RGB distance between color1 and color2. """
  return np.sqrt(np.sum((color2 - color1)**2))

#------------------------------------------------------------------------------

class ImageLibrary:
  def __init__(self, path, N, color):
    """Construct a libraby of images from the given path, downsampling each
    image to be N x N pixels, with color dimension 3. 
    If color = true, uses RGB, otherwise, use greyscale-like RGB """
    self.PicDic = []
    self.LoadLibrary(path, N, color)

  def LoadLibrary(self, path, N, color):
    """Loads images into a dictionary with keys of grayscale value, and values
    of the downsampled NxN image."""
    ## Get the list of images in the path
    for name in [os.path.join(path, f) for f in os.listdir(path)]:
      ## Open and resize the image
      img = Im.open(name).resize((N,N))
      ## Convert to rgb
      if (color):
        img = img.convert('RGB')
      ## else, convert to grayscale and then convert to rgb
      else:
        img = img.convert('L').convert('RGB')
      key = np.array([np.mean(np.array(img)[:,:,i]) for i in range(3)])
      self.PicDic.append((key, np.array(img)))

  def FindNearest(self, data):
    """Given cell data, return the closest image in rgb distance
    as an N x N x 3 (color dim) numpy array.
    """
    ## Compute RGB mean of data
    value = np.array([np.mean(data[:,:,i]) for i in range(3)])
    return min(self.PicDic, key=lambda y: RGBDistance(y[0],value))[1]

def main():
  IL = ImageLibrary('Blocks', 100, True)
  a = IL.FindNearest([30, 40, 50])
  print("Returned shape ", a.shape)

if __name__ == "__main__":
  main()
